Fractionation on the level of authors: wos_b_202404 and scp_b_202404
================
Stephan Stahlschmidt, Marion Schmidt and Dimity Stephen
11 Dezember, 2024

- [Motivation](#motivation)
- [Author information in the KB database
  schema](#author-information-in-the-kb-database-schema)
- [Potential Solution](#potential-solution)
- [WoS: wos_b_202404](#wos-wos_b_202404)
  - [Data Quality](#data-quality)
  - [Encoding of information on
    authors](#encoding-of-information-on-authors)
  - [Implementation on national
    level](#implementation-on-national-level)
    - [Testing](#testing)
  - [Known Caveats](#known-caveats)
- [Scopus: scp_b_202404](#scopus-scp_b_202404)
  - [Data Quality](#data-quality-1)
  - [Encoding of information on
    authors](#encoding-of-information-on-authors-1)
  - [Implementation on national
    level](#implementation-on-national-level-1)
    - [Testing](#testing-1)
  - [Known Caveats](#known-caveats-1)
- [Summing up](#summing-up)
- [Appendix](#appendix)
  - [KB institution encoding](#kb-institution-encoding)
    - [Implementation for
      wos_b_202404](#implementation-for-wos_b_202404)
      - [Testing](#testing-2)
    - [Implementation for
      scopus_b_202404](#implementation-for-scopus_b_202404)
      - [Testing](#testing-3)
- [Literatur](#literatur)

# Motivation

Waltman and Eck (2015) demonstrate that whole counting is incompatible
with field normalization. They propose fractional counting on the level
of authors implemented by fractional counting on address strings.

In any such approach two issues arise:

1.  Several authors might be affiliated to the same organisation
2.  Single authors might be affiliated with several organisations

Example:

WoS:

``` sql
SELECT DISTINCT au_af.item_id,
  i_au.author_seq_nr,
  i_au.family_name,
  i_au.given_name,
  i_af.organization,
  i_af.type,
  corresponding,
  countrycode
FROM wos_b_202404.authors_affiliations au_af 
JOIN wos_b_202404.items_authors i_au
  ON (au_af.item_id, au_af.author_seq_nr) = (i_au.item_id, i_au.author_seq_nr)
JOIN wos_b_202404.items_affiliations i_af
  ON (i_af.item_id, i_af.aff_seq_nr) = (au_af.item_id, au_af.aff_seq_nr)
WHERE au_af.item_id = 'WOS:000262133500009'
ORDER BY   i_au.family_name,
  i_au.given_name,
  i_af.type
```

<div class="knitsql-table">

| item_id             | author_seq_nr | family_name | given_name | organization                                              | type | corresponding | countrycode |
|:--------------------|--------------:|:------------|:-----------|:----------------------------------------------------------|:-----|:--------------|:------------|
| WOS:000262133500009 |             3 | Masek       | Karel      | {“Charles University Faculty of Mathematics and Physics”} | RS   | FALSE         | CZE         |
| WOS:000262133500009 |             4 | Matolin     | Vladimir   | {“Charles University Faculty of Mathematics and Physics”} | RS   | FALSE         | CZE         |
| WOS:000262133500009 |             2 | Skala       | Tomas      | {“Charles University Faculty of Mathematics and Physics”} | RS   | TRUE          | CZE         |
| WOS:000262133500009 |             2 | Skala       | Tomas      | {“Sincrotrone Trieste SCpA”}                              | RS   | TRUE          | ITA         |
| WOS:000262133500009 |             2 | Skala       | Tomas      | {“Sincrotrone Trieste SCpA”}                              | RP   | TRUE          | ITA         |
| WOS:000262133500009 |             1 | Sutara      | Frantisek  | {“Charles University Faculty of Mathematics and Physics”} | RS   | FALSE         | CZE         |
| WOS:000262133500009 |             1 | Sutara      | Frantisek  | {IPN}                                                     | RS   | FALSE         | MEX         |

7 records

</div>

SCP:

``` sql
SELECT DISTINCT au_af.item_id,
  i_au.author_seq_nr,
  i_au.family_name,
  i_au.given_name,
  i_af.organization,
  i_af.type,
  corresponding,
  countrycode
FROM scp_b_202404.authors_affiliations au_af 
JOIN scp_b_202404.items_authors i_au
  ON (au_af.item_id, au_af.author_seq_nr) = (i_au.item_id, i_au.author_seq_nr)
JOIN scp_b_202404.items_affiliations i_af
  ON (i_af.item_id, i_af.aff_seq_nr) = (au_af.item_id, au_af.aff_seq_nr)
    AND i_af.type = 'RS'
WHERE au_af.item_id = '2-s2.0-56949097588'
ORDER BY   i_au.family_name,
  i_au.given_name,
  i_af.type
```

<div class="knitsql-table">

| item_id            | author_seq_nr | family_name | given_name | organization                                                                                           | type | corresponding | countrycode |
|:-------------------|--------------:|:------------|:-----------|:-------------------------------------------------------------------------------------------------------|:-----|:--------------|:------------|
| 2-s2.0-56949097588 |             3 | Mašek       | Karel      | {“Charles University”,“Faculty of Mathematics and Physics”,“Department of Surface and Plasma Science”} | RS   | FALSE         | CZE         |
| 2-s2.0-56949097588 |             4 | Matolín     | Vladimír   | {“Charles University”,“Faculty of Mathematics and Physics”,“Department of Surface and Plasma Science”} | RS   | FALSE         | CZE         |
| 2-s2.0-56949097588 |             2 | Skála       | Tomáš      | {“Charles University”,“Faculty of Mathematics and Physics”,“Department of Surface and Plasma Science”} | RS   | TRUE          | CZE         |
| 2-s2.0-56949097588 |             2 | Skála       | Tomáš      | {“Sincrotrone Trieste SCpA”}                                                                           | RS   | TRUE          | ITA         |
| 2-s2.0-56949097588 |             1 | Šutara      | František  | {“Charles University”,“Faculty of Mathematics and Physics”,“Department of Surface and Plasma Science”} | RS   | FALSE         | CZE         |
| 2-s2.0-56949097588 |             1 | Šutara      | František  | {“Physics Department”,CINVESTAV-IPN}                                                                   | RS   | FALSE         | MEX         |

6 records

</div>

Authors might be assigned to several sub-organisations, while we focus
on the level of organisations. Hence a `DISTICT` is applied.

# Author information in the KB database schema

An author on a paper is identified by a combination of the `item_id` and
order of appearance, i.e. `author_seq_nr`.

The affiliations of the *reprint* `RP` and *research* `RS` authors are
listed in `items_affiliations`, as up to publication year 1998 WoS `RS`
addresses matching a `RP` address were not included and hence both the
*reprint* affiliations and the *researcher* affiliations are needed to
cover all affiliations. The filter `type='RP'` in `items_affiliations`
should solve this double counting issue for all publication years after
1998. Hence we focus on the publications years 2000+.

Due to the prominent role of corresponding authors paying the article
processing charges (APC) in the open access transformation, articles may
not hold only one, but several `RP` authors.

# Potential Solution

Fractional counting based on author attribution and subsequent
aggregation to the level of interest (here: countries) might be computed
as follows:

``` sql
SELECT item_id, /* Aggregating on country level */
    countrycode,
    SUM(orga_frak) AS cntry_frac
FROM( /* Aggregating on organization level */
    SELECT item_id,
        organization,
        countrycode,
        SUM(orga_share)  AS orga_frak
    FROM( /* Computing fractional contribution of organsation on author level accounting for potentially multiple affiliations by assigning equal weight to each author and divide author's weight by number of the author's affiliations */
        SELECT item_id, 
            author_seq_nr,
            organization,
            countrycode,
            1/(MAX(author_seq_nr) OVER (PARTITION BY item_id))::numeric/(dense_rank() OVER(PARTITION BY item_id, author_seq_nr ORDER BY organization ASC) + dense_rank () OVER (PARTITION BY item_id, author_seq_nr ORDER BY organization DESC) - 1)::numeric AS orga_share
        FROM( /* Appling DISTINCT to account for sub-orgnaisations */
            SELECT DISTINCT au_af.item_id,
                au_af.author_seq_nr,
                au_af.organization,
                countrycode
            FROM wos_b_202404.authors_affiliations au_af 
            JOIN wos_b_202404.items_authors i_au
                ON (au_af.item_id, au_af.author_seq_nr) = (i_au.item_id, i_au.author_seq_nr)
            JOIN wos_b_202404.items_affiliations i_af
                ON (i_af.item_id, i_af.aff_seq_nr) = (au_af.item_id, au_af.aff_seq_nr)
                AND i_af.type = 'RS'
            WHERE au_af.item_id = 'WOS:000262133500009'
        ) tmp1
    ) tmp2
    GROUP BY item_id, organization, countrycode
) tmp3
GROUP BY item_id, countrycode;
```

<div class="knitsql-table">

| item_id             | countrycode | cntry_frac |
|:--------------------|:------------|-----------:|
| WOS:000262133500009 | MEX         |      0.125 |
| WOS:000262133500009 | CZE         |      0.750 |
| WOS:000262133500009 | ITA         |      0.125 |

3 records

</div>

# WoS: wos_b_202404

## Data Quality

Necessary information in variables like `organization`, `author_seq_nr`
and `type` might not always be available and especially before 2008 the
link between authors and their affiliated organisation was most often
only recorded for the corresponding author resulting in an incomplete
table `authors_affiliations`.

<!-- ```{r, echo=FALSE} -->
<!-- # all potential missing information jointly -->
<!-- paperOverall <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT item_id) -->
<!--   FROM( -->
<!--     SELECT DISTINCT pubyear, item_id -->
<!--     FROM ( -->
<!--       SELECT * -->
<!--       FROM wos_b_202404.items -->
<!--       WHERE source_type = 'Journal' -->
<!--       AND item_type && ARRAY['Article', 'Review'] -->
<!--       AND pubyear BETWEEN 2000 AND 2023 -->
<!--     ) it -->
<!--     JOIN wos_b_202404.authors_affiliations aa -->
<!--       ON it.item_id = aa.item_id -->
<!--     WHERE (author_sq_nr IS NULL) -->
<!--       OR (aff_seq_nr IS NULL) -->
<!--       OR (organisation IS NULL) -->
<!--     UNION -->
<!--     SELECT DISTINCT pubyear, item_id -->
<!--     FROM ( -->
<!--       SELECT * -->
<!--       FROM wos_b_202404.items -->
<!--       WHERE source_type = 'Journal' -->
<!--       AND item_type && ARRAY['Article', 'Review'] -->
<!--       AND pubyear BETWEEN 2000 AND 2023 -->
<!--     ) it -->
<!--     JOIN wos_b_202404.items_affiliations iaf -->
<!--       ON iaf.item_id = it.item_id -->
<!--     WHERE iaf.type IS NULL -->
<!--   ) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->

Percentages of `item_id` with partially missing information:

|      | authors_affiliations: no entry | authors_affiliations: no author_seq_nr | authors_affiliations: no aff_seq_nr | authors_affiliations: no organization | items_affiliations: no type | Author w/o affiliation | Affiliation w/o author |
|:-----|-------------------------------:|---------------------------------------:|------------------------------------:|--------------------------------------:|----------------------------:|-----------------------:|-----------------------:|
| 2000 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   99.0 |                   95.1 |
| 2001 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   99.0 |                   95.4 |
| 2002 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   99.0 |                   95.5 |
| 2003 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   99.0 |                   95.8 |
| 2004 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   98.9 |                   96.1 |
| 2005 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   98.7 |                   96.0 |
| 2006 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   98.5 |                   96.1 |
| 2007 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   88.0 |                   85.6 |
| 2008 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   13.3 |                   11.2 |
| 2009 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   12.0 |                   10.3 |
| 2010 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   11.0 |                    9.4 |
| 2011 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                   10.2 |                    8.8 |
| 2012 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    9.7 |                    8.4 |
| 2013 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    8.8 |                    7.6 |
| 2014 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    8.0 |                    7.1 |
| 2015 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    6.1 |                    5.2 |
| 2016 |                              0 |                                      0 |                                   0 |                                   0.4 |                           0 |                    1.0 |                    0.2 |
| 2017 |                              0 |                                      0 |                                   0 |                                   0.4 |                           0 |                    0.8 |                    0.2 |
| 2018 |                              0 |                                      0 |                                   0 |                                   0.4 |                           0 |                    0.7 |                    0.2 |
| 2019 |                              0 |                                      0 |                                   0 |                                   0.3 |                           0 |                    0.6 |                    0.1 |
| 2020 |                              0 |                                      0 |                                   0 |                                   0.3 |                           0 |                    0.5 |                    0.1 |
| 2021 |                              0 |                                      0 |                                   0 |                                   0.3 |                           0 |                    0.5 |                    0.1 |
| 2022 |                              0 |                                      0 |                                   0 |                                   0.5 |                           0 |                    0.6 |                    0.1 |
| 2023 |                              0 |                                      0 |                                   0 |                                   1.2 |                           0 |                    0.5 |                    0.1 |

![](README_files/figure-gfm/tab_friction-1.png)<!-- -->

<!-- ```{r tab_dif_author_counting, echo = FALSE, eval = FALSE} -->
<!-- # Differenz zwischen author_cnt und Zählung der fk_authors -->
<!-- dif_author_counting <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, SUM(diff) -->
<!--   FROM( -->
<!--     SELECT DISTINCT fk_items, pubyear, -->
<!--       CASE  WHEN author_cnt = (COUNT(DISTINCT fk_authors) OVER (PARTITION BY fk_items)) -->
<!--               THEN 0 -->
<!--             ELSE 1 -->
<!--       END diff -->
<!--     FROM wos_b_2018.items it -->
<!--     JOIN wos_b_2018.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--     WHERE pubtype = 'Journal' -->
<!--       AND doctype IN ('Article', 'Review') -->
<!--       AND pubyear BETWEEN 2007 AND 2017 -->
<!--       AND type = 'RS' -->
<!--       AND role = 'author' -->
<!--   ) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- Prozentualer Anteil von pk_items mit Unterschieden in author_cnt und Zählung der zugehörigen fk_authors: -->
<!-- ```{r tab_diff, echo = FALSE, eval = FALSE} -->
<!-- tmp <- as.matrix(dif_author_counting[,2]/papersByYear[,2] * 100) -->
<!-- rownames(tmp) <- as.character(2007:2017) -->
<!-- kable(tmp, -->
<!--       digits = 1, -->
<!--       row.names = TRUE, -->
<!--       col.names=c("# Autoren != author_cnt")) -->
<!-- matplot(2007:2017, tmp, -->
<!--      xlim=c(2007,2017), -->
<!--      ylim=c(0,100), -->
<!--      lwd=2, -->
<!--      lty=1, -->
<!--      bty="n", -->
<!--      xlab="", -->
<!--      ylab="%", -->
<!--      type="l", -->
<!--      col=viridis(2)) -->
<!-- # legend("topright", -->
<!-- #        legend = c("# Autoren != author_cnt", "# Affiliationen != inst_cnt"), -->
<!-- #        fill=viridis(2), -->
<!-- #        bty = "n") -->
<!-- ``` -->

## Encoding of information on authors

As stated [above](#author-types-in-wos-and-scp) the *KB* Schema uses
different ways to encode information on the reprint author and research
authors. Furthermore this encoding of information on the authors might
be affected by missing crucial information.

Hence, we observe the share of papers (i.e. articles and reviews in
journals) with exact one corresponding author (`RP`: Reprint author),
several corresponding authors, no corresponding author and no ordinary
author (`RS`: Research author). This information is extended by the
share of papers with double appearance of at least one author name,
implying the double entries for the corresponding author. In the last
column the share of papers with no assigned author, but also no missing
information on potential authors is presented highlighting a data
quality issue or erosion of the author concept.

|      | one corresponding author | several corresponding authors | no corresponding author | no ordinary author |
|:-----|-------------------------:|------------------------------:|------------------------:|-------------------:|
| 2000 |                     93.3 |                           0.1 |                     6.7 |               15.9 |
| 2001 |                     92.6 |                           0.1 |                     7.3 |               15.0 |
| 2002 |                     92.7 |                           0.1 |                     7.3 |               14.3 |
| 2003 |                     93.6 |                           0.1 |                     6.4 |               13.7 |
| 2004 |                     95.7 |                           0.1 |                     4.2 |               13.1 |
| 2005 |                     96.1 |                           0.1 |                     3.8 |               12.5 |
| 2006 |                     96.8 |                           0.1 |                     3.2 |               12.0 |
| 2007 |                     96.9 |                           0.1 |                     3.0 |               11.7 |
| 2008 |                     97.1 |                           0.1 |                     2.8 |               11.8 |
| 2009 |                     97.5 |                           0.1 |                     2.4 |               11.5 |
| 2010 |                     97.7 |                           0.1 |                     2.2 |               11.0 |
| 2011 |                     98.0 |                           0.1 |                     1.8 |               10.4 |
| 2012 |                     98.1 |                           0.1 |                     1.8 |                9.8 |
| 2013 |                     98.3 |                           0.1 |                     1.6 |                9.2 |
| 2014 |                     98.6 |                           0.1 |                     1.2 |                8.8 |
| 2015 |                     98.7 |                           0.2 |                     1.2 |                8.4 |
| 2016 |                     89.7 |                           9.3 |                     0.9 |                9.7 |
| 2017 |                     89.6 |                           9.6 |                     0.7 |                8.2 |
| 2018 |                     88.6 |                          10.8 |                     0.6 |                7.8 |
| 2019 |                     87.1 |                          12.4 |                     0.5 |                7.4 |
| 2020 |                     86.0 |                          13.5 |                     0.5 |                6.9 |
| 2021 |                     84.7 |                          14.8 |                     0.5 |                6.5 |
| 2022 |                     82.5 |                          16.9 |                     0.6 |                6.2 |
| 2023 |                     81.8 |                          17.7 |                     0.5 |                6.0 |

![](README_files/figure-gfm/tab_author-1.png)<!-- -->

## Implementation on national level

Due to large shares of items with incomplete information we propose a
hybrid approach. We (1) fractionalize on the author level (and
subsequently aggregate to the country level) for every `item_id` with
complete information in the `items_authors_institutions` table, (2)
apply fractionalization on the organisation level (and therefore ignore
any authors) via `organization` for all `item_id` with an affiliation
information for at least one `RS` author and (3) apply the whole count
to the country of the corresponding author (`RP`) if no affiliations for
the ordinary `RS` authors is available. This last case occurs especially
before 1998. Hence we follow Waltman and Eck (2015) but account for data
quality issues at the same time.

In detail we compute a table which holds country specific weights for
fractional counting. Lower aggregates levels like organisations might
also be computed.

Identify publications with complete and incomplete RS/RP information:

``` sql
DROP TABLE tmp_items_iai_complete_wos;

/*
Create temporaray table as an indicator of items with complete iai record:
*/
CREATE TABLE tmp_items_iai_complete_wos (
    item_id VARCHAR(50),
    ind SMALLINT
);

/*
Definition for ind:
1 := items with COMPLETE iai information
2 := items with INCOMPLETE iai information and feasible link to institution table for RS author
3 := items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/

/*
populate temporary table with indicator category ind = 1 (items with COMPLETE iai information)
*/
INSERT INTO tmp_items_iai_complete_wos (
WITH it AS (
  SELECT item_id
  FROM wos_b_202404.items
  WHERE source_type = 'Journal'
    AND item_type && ARRAY['Article', 'Review']
    AND pubyear BETWEEN 2000 AND 2023
)
SELECT DISTINCT tmp_aa.item_id, 1 -- subset of items with complete iai information
FROM( -- counts numbers of author and affiliation entires in aa for every item
    SELECT DISTINCT it.item_id,
      MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_max,
      MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_max
    FROM it
    LEFT JOIN wos_b_202404.authors_affiliations aa
      ON aa.item_id = it.item_id
) tmp_aa
JOIN( -- counts numbers of authors in iau
    SELECT it.item_id,
      MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_cnt
    FROM it
    JOIN wos_b_202404.items_authors iau
      ON iau.item_id = it.item_id
) tmp_au
  ON tmp_au.item_id = tmp_aa.item_id
    AND tmp_au.author_cnt = tmp_aa.author_max -- compare both numbers
JOIN( -- counts numbers of affiliationa in iaf
    SELECT it.item_id,
      MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_cnt
    FROM it
    JOIN wos_b_202404.items_affiliations iaf
      ON iaf.item_id = it.item_id
        AND iaf.type = 'RS'
) tmp_af
  ON tmp_af.item_id = tmp_aa.item_id
    AND tmp_af.aff_cnt = tmp_aa.aff_max); -- compare both numbers

/*
populate temporary table with indicator category ind = 2 (items with INCOMPLETE iai information but affiliation for RS author) and ind = 3 (items with INCOMPLETE iai information but affiliation for RP author only)
*/
INSERT INTO tmp_items_iai_complete_wos (
WITH it AS(
  SELECT item_id
  FROM wos_b_202404.items
  WHERE source_type = 'Journal'
    AND item_type && ARRAY['Article', 'Review']
    AND pubyear BETWEEN 2000 AND 2023
)
SELECT DISTINCT it.item_id,
    CASE
        WHEN COUNT(aff_seq_nr) OVER (PARTITION BY it.item_id) >= 1
            THEN 2
        WHEN COUNT(aff_seq_nr) OVER (PARTITION BY it.item_id) = 0
            THEN 3
    END
FROM it
LEFT JOIN wos_b_202404.items_affiliations iaf
  ON iaf.item_id = it.item_id
    AND type = 'RS'
WHERE it.item_id NOT IN (
    SELECT DISTINCT tmp_aa.item_id -- subset of items with complete iai information
    FROM( -- counts numbers of author and affiliation entries in aa for every item
        SELECT DISTINCT it.item_id,
          MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_max,
          MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_max
        FROM it
        LEFT JOIN wos_b_202404.authors_affiliations aa
          ON aa.item_id = it.item_id
    ) tmp_aa
    JOIN( -- counts numbers of authors in iau
        SELECT it.item_id,
          MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_cnt
        FROM it
        JOIN wos_b_202404.items_authors iau
          ON iau.item_id = it.item_id
    ) tmp_au
      ON tmp_au.item_id = tmp_aa.item_id
        AND tmp_au.author_cnt = tmp_aa.author_max -- compare both numbers
    JOIN( -- counts numbers of affiliationa in iaf
        SELECT it.item_id,
          MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_cnt
        FROM it
        JOIN wos_b_202404.items_affiliations iaf
          ON iaf.item_id = it.item_id
            AND iaf.type = 'RS'
    ) tmp_af
      ON tmp_af.item_id = tmp_aa.item_id
        AND tmp_af.aff_cnt = tmp_aa.aff_max) -- compare both numbers
);

-- create index:
CREATE INDEX tmp_items_iai_complete_wos_ix ON tmp_items_iai_complete_wos (item_id, ind);
```

Compute equal weights/fractions by country for each paper:

``` sql
/*
create empty table defining fractional weight for every paper by country
causes implicit commit
*/
DROP TABLE frctnl_cnt_wosb202404_cntrylvl;

CREATE TABLE frctnl_cnt_wosb202404_cntrylvl (
    item_id VARCHAR(50),
    countrycode VARCHAR(10),
    frac_share NUMERIC(5,4),
    ind_complete SMALLINT
);

/*
compute weight for items with COMPLETE iai information via fractional counting on author level:
*/
INSERT INTO frctnl_cnt_wosb202404_cntrylvl
SELECT item_id, countrycode, SUM(orga_frak) AS frac_share, 1 -- aggregating on country level
FROM( -- aggregating on organization1 level
    SELECT item_id, organization, countrycode, SUM(orga_share) AS orga_frak
    FROM( -- computing fractional contribution of organsation on author level
      SELECT DISTINCT it.item_id,
        aa.organization,
        iaf.countrycode,
        aa.author_seq_nr,
        (1/(MAX(author_seq_nr) OVER (PARTITION BY aa.item_id))::numeric)/(DENSE_RANK() OVER(PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization ASC) + DENSE_RANK() OVER (PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization DESC) - 1) AS orga_share
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_wos
        WHERE ind = 1
        ) it
      JOIN wos_b_202404.authors_affiliations aa
        ON aa.item_id = it.item_id
          AND organization IS NOT NULL
      JOIN wos_b_202404.items_affiliations iaf
        ON iaf.item_id = aa.item_id
          AND iaf.aff_seq_nr = aa.aff_seq_nr
          AND iaf.type = 'RS'
      )
    GROUP BY item_id, organization, countrycode
)
GROUP BY item_id, countrycode, 1;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for RS author
*/
INSERT INTO frctnl_cnt_wosb202404_cntrylvl
SELECT item_id, countrycode, SUM(orga_frak) AS frac_share, 2 -- aggregating on country level
FROM( -- aggregating on organization level
    SELECT DISTINCT iaf.item_id,
      iaf.organization,
      iaf.countrycode,
      1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
    FROM (
      SELECT item_id
      FROM tmp_items_iai_complete_wos
      WHERE ind = 2
    ) it
    JOIN wos_b_202404.items_affiliations iaf
      ON iaf.item_id = it.item_id
        AND iaf.type = 'RS'
        AND organization IS NOT NULL
  )
GROUP BY item_id, countrycode, 2;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/
INSERT INTO frctnl_cnt_wosb202404_cntrylvl
SELECT DISTINCT iaf.item_id,
  iaf.countrycode,
  1/DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.countrycode ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.countrycode DESC) - 1 AS frac_share, -- account for several RP authors from different countries
  3
FROM (
  SELECT item_id
  FROM tmp_items_iai_complete_wos
  WHERE ind = 3
) it
JOIN wos_b_202404.items_affiliations iaf
  ON iaf.item_id = it.item_id
    AND iaf.type = 'RP'
    AND countrycode IS NOT NULL;

CREATE INDEX frctnl_cnt_wosb202404_cntry_ix ON frctnl_cnt_wosb202404_cntrylvl (item_id, countrycode);

COMMIT;
```

### Testing

We run several test on the computed weights to observe their coverage
and their properness to serve as weights. The following numbers present
the respective percentage shares of indexed articles and reviews in
journals with questionable weights generated by the implementation for
*Web Of Science* [detailed above](#implementation-on-national-level).

|      | No weights available? | Inappropriate weights, i.e. smaller or bigger than (0,1\]? | More than one weight by country? | Weights missing for some country? | Sum of weights bigger than one? | Sum of weights smaller than one? |
|:-----|----------------------:|-----------------------------------------------------------:|---------------------------------:|----------------------------------:|--------------------------------:|---------------------------------:|
| 2000 |                   4.7 |                                                       43.2 |                                0 |                               1.3 |                             0.0 |                             43.2 |
| 2001 |                   4.2 |                                                       45.0 |                                0 |                               1.2 |                             0.0 |                             45.0 |
| 2002 |                   4.1 |                                                       46.6 |                                0 |                               1.2 |                             0.0 |                             46.6 |
| 2003 |                   3.7 |                                                       48.6 |                                0 |                               1.0 |                             0.0 |                             48.7 |
| 2004 |                   3.2 |                                                       50.2 |                                0 |                               0.9 |                             0.0 |                             50.2 |
| 2005 |                   3.2 |                                                       51.0 |                                0 |                               0.9 |                             0.0 |                             51.1 |
| 2006 |                   2.8 |                                                       52.1 |                                0 |                               0.8 |                             0.0 |                             52.1 |
| 2007 |                   2.7 |                                                       47.2 |                                0 |                               0.7 |                             0.1 |                             47.7 |
| 2008 |                   2.4 |                                                        3.4 |                                0 |                               0.7 |                             0.5 |                              7.5 |
| 2009 |                   2.0 |                                                        3.1 |                                0 |                               0.6 |                             0.5 |                              7.6 |
| 2010 |                   1.8 |                                                        3.1 |                                0 |                               0.6 |                             0.6 |                              7.5 |
| 2011 |                   1.5 |                                                        2.9 |                                0 |                               0.6 |                             0.6 |                              7.1 |
| 2012 |                   1.5 |                                                        2.8 |                                0 |                               0.5 |                             0.7 |                              7.0 |
| 2013 |                   1.3 |                                                        2.1 |                                0 |                               0.5 |                             0.7 |                              4.4 |
| 2014 |                   1.1 |                                                        1.9 |                                0 |                               0.4 |                             0.8 |                              3.8 |
| 2015 |                   1.0 |                                                        1.8 |                                0 |                               0.4 |                             0.9 |                              3.6 |
| 2016 |                   1.0 |                                                        1.6 |                                0 |                               0.2 |                             1.0 |                              3.4 |
| 2017 |                   0.8 |                                                        1.6 |                                0 |                               0.2 |                             1.1 |                              3.6 |
| 2018 |                   0.7 |                                                        1.5 |                                0 |                               0.1 |                             1.1 |                              3.2 |
| 2019 |                   0.6 |                                                        1.0 |                                0 |                               0.1 |                             1.2 |                              2.4 |
| 2020 |                   0.5 |                                                        1.0 |                                0 |                               0.1 |                             1.3 |                              2.5 |
| 2021 |                   0.5 |                                                        1.1 |                                0 |                               0.1 |                             1.4 |                              2.8 |
| 2022 |                   0.6 |                                                        1.2 |                                0 |                               0.1 |                             1.4 |                              3.3 |
| 2023 |                   0.5 |                                                        1.3 |                                0 |                               0.1 |                             1.4 |                              3.8 |

![](README_files/figure-gfm/tab_testing_wos_cntrylvl-1.png)<!-- -->

<!-- ## Comparison with ISI fractionation on organization1 -->
<!-- As an alternative implementation we also compute weights based on fractionation on the `organization1` level: -->
<!-- ```sql -->
<!-- /* -->
<!-- create empty table defining fractional weight for every paper by country -->
<!-- based on fractionation on organization1 level -->
<!-- */ -->
<!-- CREATE TABLE frctnl_cnt_wosb2020_ISIorg1 ( -->
<!--     fk_items NUMBER, -->
<!--     countrycode VARCHAR2(10 CHAR), -->
<!--     frac_share NUMBER(3,2) -->
<!-- ); -->
<!-- /* -->
<!-- include items with INCOMPLETE iai information via fractional counting on organization level: -->
<!-- */ -->
<!-- INSERT INTO frctnl_cnt_wosb2020_ISIorg1 -->
<!-- SELECT fk_items, countrycode, SUM(orga_frak) AS frac_share -- aggregating on country level -->
<!-- FROM( -- aggregating on organization1 level -->
<!--     SELECT fk_items, organization1, countrycode, SUM(orga_share)  AS orga_frak -->
<!--     FROM( -- computing fractional contribution on organisation level -->
<!--       SELECT DISTINCT fk_items, ORGANIZATION1, countrycode, -->
<!--         (1/(COUNT (DISTINCT organization1) OVER (PARTITION BY fk_items))) AS orga_share -->
<!--       FROM wos_b_2020.items it -->
<!--       JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--       JOIN wos_b_2020.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--       WHERE pubtype = 'Journal' -->
<!--         AND doctype IN ('Article', 'Review') -->
<!--         AND pubyear = 2017 -->
<!--         AND (role = 'author' OR role IS NULL) -->
<!--         AND type = 'RS' -->
<!--         AND organization1 IS NOT NULL -->
<!--       ) -->
<!--     GROUP BY fk_items, organization1, countrycode -->
<!-- ) -->
<!-- GROUP BY fk_items, countrycode; -->
<!-- CREATE INDEX frctnl_cnt_wosb2020_ISIorg1_ix ON frctnl_cnt_wosb2020_ISIorg1 (fk_items, countrycode); -->
<!-- COMMIT; -->
<!-- ``` -->
<!-- Comparison of national publication counts and highly cited rates based on hybrid approach and the ISI approach of pure `organization1` fractional counts. -->
<!-- ### National Fractional Publication Counts -->
<!-- ```{r, ISIcompa-pubcnt-data, echo=FALSE} -->
<!-- # hybrid approach -->
<!-- pubs_cntry_whole_wos_frac <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--     SELECT country, pubyear, SUM(frac_share) pub_cnt_frak -->
<!--     FROM( -->
<!--       SELECT DISTINCT it.pubyear, inst.countrycode country, it.pk_items, frak.frac_share -->
<!--       FROM wos_b_2020.items it -->
<!--       JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--         AND (iai.role = 'author' OR iai.role IS NULL) -->
<!--       JOIN wos_b_2020.institutions inst ON inst.pk_institutions=iai.fk_institutions -->
<!--       JOIN frctnl_cnt_wosb2020_cntrylvl frak ON frak.fk_items = it.pk_items -->
<!--         AND frak.countrycode = inst.countrycode -->
<!--       WHERE it.pubyear = 2017 -->
<!--         AND it.doctype IN ('Article','Review') -->
<!--         AND it.pubtype = 'Journal' -->
<!--         AND inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA',  -->
<!--                                    'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--     ) -->
<!--     GROUP BY country, pubyear -->
<!--     ORDER BY pub_cnt_frak DESC -->
<!--     "), -->
<!--     width = 100000, simplify=TRUE) -->
<!-- ) -->
<!-- # ISI approach -->
<!-- pubs_cntry_whole_wos_frac_ISI <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--     SELECT country, pubyear, SUM(frac_share) pub_cnt_frak_ISI -->
<!--     FROM( -->
<!--       SELECT DISTINCT it.pubyear, inst.countrycode country, it.pk_items, frak.frac_share -->
<!--       FROM wos_b_2020.items it -->
<!--       JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--         AND (iai.role = 'author' OR iai.role IS NULL) -->
<!--       JOIN wos_b_2020.institutions inst ON inst.pk_institutions=iai.fk_institutions -->
<!--       JOIN frctnl_cnt_wosb2020_ISIorg1 frak ON frak.fk_items = it.pk_items -->
<!--         AND frak.countrycode = inst.countrycode -->
<!--       WHERE it.pubyear = 2017 -->
<!--         AND it.doctype IN ('Article','Review') -->
<!--         AND it.pubtype = 'Journal' -->
<!--         AND inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA',  -->
<!--                                    'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--     ) -->
<!--     GROUP BY country, pubyear -->
<!--     ORDER BY pub_cnt_frak_ISI DESC -->
<!--     "), -->
<!--     width = 100000, simplify=TRUE) -->
<!-- ) -->
<!-- ``` -->
<!-- ```{r, ISIcompa-pubcnt-table, echo=FALSE, cache=FALSE} -->
<!-- # table -->
<!-- pubs_cnt_compa <- cbind(pubs_cntry_whole_wos_frac, pubs_cntry_whole_wos_frac_ISI) -->
<!-- kable(pubs_cnt_compa) -->
<!-- # kable(pubs_cnt_compa[1,3,4,6], -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac_ISI, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- ``` -->
<!-- ### National Excellence Rate in 2017 -->
<!-- ```{r, ISIcompa-hc-data, echo=FALSE} -->
<!-- # hybrid approach -->
<!-- er_cntry_compa <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT countrycode country, SUM(frac_share*in_10p) / SUM(frac_share) * 100 hc_share -->
<!--   FROM ( -->
<!--       SELECT DISTINCT inst.countrycode, it.pubyear, it.pk_items, frac_share, in_hc_3y in_10p -->
<!--            FROM wos_b_2020.items it  -->
<!--            JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items = iai.fk_items -->
<!--                AND (iai.role = 'author' OR iai.role IS NULL) -->
<!--                AND type = 'RS' -->
<!--            JOIN wos_b_2020.institutions inst ON iai.fk_institutions = inst.pk_institutions -->
<!--            JOIN wos_b_2020.d_v_items_hc th ON th.pk_items = it.pk_items -->
<!--               AND th.percentile = 0.1 -->
<!--            JOIN dzhwsstahlschmidt.frctnl_cnt_wosb2020_cntrylvl frac ON frac.fk_items = it.pk_items -->
<!--                AND frac.countrycode = inst.countrycode -->
<!--             WHERE inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA',  -->
<!--                                                              'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--                     AND it.pubyear = 2017 -->
<!--                AND it.pubtype = 'Journal' -->
<!--                AND it.doctype IN ('Article', 'Review') -->
<!--         ) -->
<!--     GROUP BY countrycode -->
<!--     ORDER BY hc_share DESC -->
<!--     "), -->
<!--    width = 100000, simplify = TRUE) -->
<!-- ) -->
<!-- # ISI approach -->
<!-- er_cntry_compa_ISI <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT countrycode country, SUM(frac_share*in_10p) / SUM(frac_share) * 100 hc_share -->
<!--   FROM ( -->
<!--       SELECT DISTINCT inst.countrycode, it.pubyear, it.pk_items, frac_share, in_hc_3y in_10p -->
<!--            FROM wos_b_2020.items it  -->
<!--            JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items = iai.fk_items -->
<!--                AND (iai.role = 'author' OR iai.role IS NULL) -->
<!--                AND type = 'RS' -->
<!--            JOIN wos_b_2020.institutions inst ON iai.fk_institutions = inst.pk_institutions -->
<!--            JOIN wos_b_2020.d_v_items_hc th ON th.pk_items = it.pk_items -->
<!--               AND th.percentile = 0.1 -->
<!--            JOIN frctnl_cnt_wosb2020_ISIorg1 frak ON frak.fk_items = it.pk_items -->
<!--               AND frak.countrycode = inst.countrycode -->
<!--             WHERE inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA',  -->
<!--                                                              'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--                     AND it.pubyear = 2017 -->
<!--                AND it.pubtype = 'Journal' -->
<!--                AND it.doctype IN ('Article', 'Review') -->
<!--         ) -->
<!--     GROUP BY countrycode -->
<!--     ORDER BY hc_share DESC -->
<!--     "), -->
<!--    width = 100000, simplify = TRUE) -->
<!-- ) -->
<!-- ``` -->
<!-- ```{r, ISIcompa-hc-table, echo=FALSE, cache=FALSE} -->
<!-- # table -->
<!-- pubs_cnt_compa <- cbind(er_cntry_compa, er_cntry_compa_ISI) -->
<!-- kable(pubs_cnt_compa) -->
<!-- # kable(pubs_cnt_compa[1,3,4,6], -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac_ISI, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- ``` -->

## Known Caveats

- The approach produces inappropriate weights for WoS-indexed
  publications before 2008. This observation does not hold within Scopus
  and could be caused by the WoS author encoding before 2008, i.e. no
  link between authors and their affiliations
- Mega author papers with a large number of participating countries are
  excluded from any analysis applying these weights if the author
  specific fractional weights become too small and are rounded to zero.
- Whenever an authors holds several affiliations of which two or more
  actually describe the same organisation, but have not been cleaned in
  `organization`, we assign erroneous weights. This issue might be less
  severe on a country level analysis and could ultimately only be
  resolved via a worldwide institution harmonization.
- Affiliations without an `organzation` are omitted, e.g. private
  addresses (1,2% in wos_b_2018)

# Scopus: scp_b_202404

## Data Quality

<!-- ```{r, echo=FALSE} -->
<!-- # all potential missing information jointly -->
<!-- paperOverall <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT item_id) -->
<!--   FROM( -->
<!--     SELECT DISTINCT pubyear, item_id -->
<!--     FROM ( -->
<!--       SELECT * -->
<!--       FROM scp_b_202404.items -->
<!--       WHERE source_type = 'Journal' -->
<!--       AND item_type && ARRAY['Article', 'Review'] -->
<!--       AND pubyear BETWEEN 2000 AND 2023 -->
<!--     ) it -->
<!--     JOIN scp_b_202404.authors_affiliations aa -->
<!--       ON it.item_id = aa.item_id -->
<!--     WHERE (author_sq_nr IS NULL) -->
<!--       OR (aff_seq_nr IS NULL) -->
<!--       OR (organisation IS NULL) -->
<!--     UNION -->
<!--     SELECT DISTINCT pubyear, item_id -->
<!--     FROM ( -->
<!--       SELECT * -->
<!--       FROM scp_b_202404.items -->
<!--       WHERE source_type = 'Journal' -->
<!--       AND item_type && ARRAY['Article', 'Review'] -->
<!--       AND pubyear BETWEEN 2000 AND 2023 -->
<!--     ) it -->
<!--     JOIN scp_b_202404.items_affiliations iaf -->
<!--       ON iaf.item_id = it.item_id -->
<!--     WHERE iaf.type IS NULL -->
<!--   ) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->

Percentages of `item_id` with partially missing information:

|      | authors_affiliations: no entry | authors_affiliations: no author_seq_nr | authors_affiliations: no aff_seq_nr | authors_affiliations: no organization | items_affiliations: no type | Author w/o affiliation | Affiliation w/o author |
|:-----|-------------------------------:|---------------------------------------:|------------------------------------:|--------------------------------------:|----------------------------:|-----------------------:|-----------------------:|
| 2000 |                              0 |                                      0 |                                   0 |                                   2.1 |                           0 |                    6.4 |                    0.3 |
| 2001 |                              0 |                                      0 |                                   0 |                                   2.1 |                           0 |                    7.8 |                    0.1 |
| 2002 |                              0 |                                      0 |                                   0 |                                   2.2 |                           0 |                    8.1 |                    0.1 |
| 2003 |                              0 |                                      0 |                                   0 |                                   2.3 |                           0 |                    8.0 |                    0.2 |
| 2004 |                              0 |                                      0 |                                   0 |                                   2.5 |                           0 |                    6.5 |                    0.1 |
| 2005 |                              0 |                                      0 |                                   0 |                                   2.5 |                           0 |                    5.0 |                    0.2 |
| 2006 |                              0 |                                      0 |                                   0 |                                   2.3 |                           0 |                    4.4 |                    0.3 |
| 2007 |                              0 |                                      0 |                                   0 |                                   2.1 |                           0 |                    4.3 |                    0.3 |
| 2008 |                              0 |                                      0 |                                   0 |                                   2.0 |                           0 |                    3.8 |                    0.3 |
| 2009 |                              0 |                                      0 |                                   0 |                                   1.4 |                           0 |                    3.6 |                    0.3 |
| 2010 |                              0 |                                      0 |                                   0 |                                   0.7 |                           0 |                    3.4 |                    0.4 |
| 2011 |                              0 |                                      0 |                                   0 |                                   0.8 |                           0 |                    3.0 |                    0.4 |
| 2012 |                              0 |                                      0 |                                   0 |                                   0.6 |                           0 |                    2.8 |                    0.3 |
| 2013 |                              0 |                                      0 |                                   0 |                                   0.4 |                           0 |                    2.7 |                    0.2 |
| 2014 |                              0 |                                      0 |                                   0 |                                   0.8 |                           0 |                    2.9 |                    0.1 |
| 2015 |                              0 |                                      0 |                                   0 |                                   0.9 |                           0 |                    2.6 |                    0.0 |
| 2016 |                              0 |                                      0 |                                   0 |                                   0.8 |                           0 |                    1.9 |                    0.0 |
| 2017 |                              0 |                                      0 |                                   0 |                                   0.8 |                           0 |                    1.4 |                    0.0 |
| 2018 |                              0 |                                      0 |                                   0 |                                   0.7 |                           0 |                    1.3 |                    0.0 |
| 2019 |                              0 |                                      0 |                                   0 |                                   0.2 |                           0 |                    1.3 |                    0.0 |
| 2020 |                              0 |                                      0 |                                   0 |                                   0.1 |                           0 |                    1.3 |                    0.1 |
| 2021 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    1.1 |                    0.1 |
| 2022 |                              0 |                                      0 |                                   0 |                                   0.0 |                           0 |                    1.0 |                    0.1 |
| 2023 |                              0 |                                      0 |                                   0 |                                   0.1 |                           0 |                    1.0 |                    0.1 |

![](README_files/figure-gfm/tab_friction_scp-1.png)<!-- -->

<!-- ```{r, tab_dif_author_counting_sc, echo=FALSE, eval=FALSE} -->
<!-- # Differenz zwischen author_cnt und Zählung der fk_authors -->
<!-- dif_author_counting_sc <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, SUM(diff) -->
<!--   FROM( -->
<!--     SELECT DISTINCT fk_items, pubyear, -->
<!--       CASE  WHEN author_cnt = (COUNT(DISTINCT fk_authors) OVER (PARTITION BY fk_items)) -->
<!--               THEN 0 -->
<!--             ELSE 1 -->
<!--       END diff -->
<!--     FROM scopus_b_2017.items it -->
<!--     JOIN scopus_b_2017.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--     WHERE pubtype = 'J' -->
<!--     AND doctype IN ('ar', 're') -->
<!--     AND pubyear BETWEEN 2007 AND 2016 -->
<!--     AND (type = 'RS' OR type IS NULL) -->
<!--   ) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- Prozentualer Anteil von pk_items mit Unterschieden in author_cnt und Zählung der zugehörigen fk_authors (Scopus): -->
<!-- ```{r, tab_diff_sc, echo=FALSE, eval=FALSE, cache=FALSE} -->
<!-- tmp <- as.matrix(dif_author_counting_sc[,2]/papersByYear_scp[,2] * 100) -->
<!-- rownames(tmp) <- as.character(2007:2016) -->
<!-- colnames(tmp) <- c("# Autoren != author_cnt") -->
<!-- kable(tmp, -->
<!--       digits = 1, -->
<!--       row.names = TRUE) -->
<!-- matplot(2007:2016, tmp, -->
<!--      xlim=c(2007,2016), -->
<!--      ylim=c(0,100), -->
<!--      lwd=2, -->
<!--      lty=1, -->
<!--      bty="n", -->
<!--      xlab="", -->
<!--      ylab="%", -->
<!--      type="l", -->
<!--      col=viridis(2)) -->
<!-- # legend("topright", -->
<!-- #        legend = c("# Autoren != author_cnt", "# Affiliation != inst_cnt"), -->
<!-- #        fill=viridis(2), -->
<!-- #        bty = "n") -->
<!-- ``` -->

## Encoding of information on authors

|      | one corresponding author | several corresponding authors | no corresponding author | no ordinary author |
|:-----|-------------------------:|------------------------------:|------------------------:|-------------------:|
| 2000 |                     84.3 |                           0.2 |                    15.5 |               18.8 |
| 2001 |                     80.4 |                           0.2 |                    19.4 |               17.9 |
| 2002 |                     81.0 |                           0.2 |                    18.7 |               17.8 |
| 2003 |                     86.6 |                           0.3 |                    13.1 |               17.6 |
| 2004 |                     86.3 |                           0.3 |                    13.4 |               16.7 |
| 2005 |                     90.2 |                           0.4 |                     9.4 |               16.3 |
| 2006 |                     93.5 |                           0.4 |                     6.1 |               16.0 |
| 2007 |                     92.5 |                           0.4 |                     7.0 |               15.2 |
| 2008 |                     78.4 |                           0.5 |                    21.1 |               12.9 |
| 2009 |                     75.2 |                           0.5 |                    24.3 |               12.3 |
| 2010 |                     73.8 |                           0.6 |                    25.6 |               12.3 |
| 2011 |                     82.1 |                           0.6 |                    17.3 |               13.1 |
| 2012 |                     89.5 |                           0.7 |                     9.8 |               13.2 |
| 2013 |                     82.4 |                           0.7 |                    16.9 |               12.4 |
| 2014 |                     82.1 |                           0.6 |                    17.3 |               11.9 |
| 2015 |                     82.8 |                           0.3 |                    16.9 |               11.5 |
| 2016 |                     83.8 |                           0.3 |                    15.9 |               10.8 |
| 2017 |                     85.8 |                           0.4 |                    13.9 |               10.2 |
| 2018 |                     87.5 |                           0.4 |                    12.1 |                9.7 |
| 2019 |                     87.6 |                           0.6 |                    11.8 |                9.5 |
| 2020 |                     84.9 |                           5.0 |                    10.1 |                9.2 |
| 2021 |                     82.8 |                           9.4 |                     7.8 |                8.8 |
| 2022 |                     81.6 |                          11.3 |                     7.1 |                8.3 |
| 2023 |                     80.7 |                          12.0 |                     7.3 |                8.1 |

![](README_files/figure-gfm/tab_author_scp-1.png)<!-- -->

## Implementation on national level

``` sql
DROP TABLE tmp_items_iai_complete_scp;

/*
Create temporaray table as an indicator of items with complete iai record:
*/
CREATE TABLE tmp_items_iai_complete_scp (
    item_id VARCHAR(50),
    ind SMALLINT
);

/*
Definition for ind:
1 := items with COMPLETE iai information
2 := items with INCOMPLETE iai information and feasible link to institution table for RS author
3 := items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/

/*
populate temporary table with indicator category ind = 1 (items with COMPLETE iai information)
*/
INSERT INTO tmp_items_iai_complete_scp (
WITH it AS (
  SELECT item_id
  FROM scp_b_202404.items
  WHERE source_type = 'Journal'
    AND item_type && ARRAY['Article', 'Review']
    AND pubyear BETWEEN 2000 AND 2023
)
SELECT DISTINCT tmp_aa.item_id, 1 -- subset of items with complete iai information
FROM( -- counts numbers of author and affiliation entires in aa for every item
    SELECT DISTINCT it.item_id,
      MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_max,
      MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_max
    FROM it
    LEFT JOIN scp_b_202404.authors_affiliations aa
      ON aa.item_id = it.item_id
) tmp_aa
JOIN( -- counts numbers of authors in iau
    SELECT it.item_id,
      MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_cnt
    FROM it
    JOIN scp_b_202404.items_authors iau
      ON iau.item_id = it.item_id
) tmp_au
  ON tmp_au.item_id = tmp_aa.item_id
    AND tmp_au.author_cnt = tmp_aa.author_max -- compare both numbers
JOIN( -- counts numbers of affiliationa in iaf
    SELECT it.item_id,
      MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_cnt
    FROM it
    JOIN scp_b_202404.items_affiliations iaf
      ON iaf.item_id = it.item_id
        AND iaf.type = 'RS'
) tmp_af
  ON tmp_af.item_id = tmp_aa.item_id
    AND tmp_af.aff_cnt = tmp_aa.aff_max); -- compare both numbers

/*
populate temporary table with indicator category ind = 2 (items with INCOMPLETE iai information but affiliation for RS author) and ind = 3 (items with INCOMPLETE iai information but affiliation for RP author only)
*/
INSERT INTO tmp_items_iai_complete_scp (
WITH it AS(
  SELECT item_id
  FROM scp_b_202404.items
  WHERE source_type = 'Journal'
    AND item_type && ARRAY['Article', 'Review']
    AND pubyear BETWEEN 2000 AND 2023
)
SELECT DISTINCT it.item_id,
    CASE
        WHEN COUNT(aff_seq_nr) OVER (PARTITION BY it.item_id) >= 1
            THEN 2
        WHEN COUNT(aff_seq_nr) OVER (PARTITION BY it.item_id) = 0
            THEN 3
    END
FROM it
LEFT JOIN scp_b_202404.items_affiliations iaf
  ON iaf.item_id = it.item_id
    AND type = 'RS'
WHERE it.item_id NOT IN (
    SELECT DISTINCT tmp_aa.item_id -- subset of items with complete iai information
    FROM( -- counts numbers of author and affiliation entries in aa for every item
        SELECT DISTINCT it.item_id,
          MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_max,
          MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_max
        FROM it
        LEFT JOIN scp_b_202404.authors_affiliations aa
          ON aa.item_id = it.item_id
    ) tmp_aa
    JOIN( -- counts numbers of authors in iau
        SELECT it.item_id,
          MAX(author_seq_nr) OVER (PARTITION BY it.item_id) AS author_cnt
        FROM it
        JOIN scp_b_202404.items_authors iau
          ON iau.item_id = it.item_id
    ) tmp_au
      ON tmp_au.item_id = tmp_aa.item_id
        AND tmp_au.author_cnt = tmp_aa.author_max -- compare both numbers
    JOIN( -- counts numbers of affiliationa in iaf
        SELECT it.item_id,
          MAX(aff_seq_nr) OVER (PARTITION BY it.item_id) AS aff_cnt
        FROM it
        JOIN scp_b_202404.items_affiliations iaf
          ON iaf.item_id = it.item_id
            AND iaf.type = 'RS'
    ) tmp_af
      ON tmp_af.item_id = tmp_aa.item_id
        AND tmp_af.aff_cnt = tmp_aa.aff_max) -- compare both numbers
);

-- create index:
CREATE INDEX tmp_items_iai_complete_scp_ix ON tmp_items_iai_complete_scp (item_id, ind);
```

Compute weights by country for each paper:

``` sql
/*
create empty table defining fractional weight for every paper by country
causes implicit commit
*/
DROP TABLE frctnl_cnt_scpb202404_cntrylvl;
CREATE TABLE frctnl_cnt_scpb202404_cntrylvl (
    item_id VARCHAR(50),
    countrycode VARCHAR(10),
    frac_share NUMERIC(5,4),
    ind_complete SMALLINT
);

/*
compute weight for items with COMPLETE iai information via fractional counting on author level:
*/
INSERT INTO frctnl_cnt_scpb202404_cntrylvl
SELECT item_id, countrycode, SUM(orga_frak) AS frac_share, 1 -- aggregating on country level
FROM( -- aggregating on organization1 level
    SELECT item_id, organization, countrycode, SUM(orga_share) AS orga_frak
    FROM( -- computing fractional contribution of organsation on author level
      SELECT DISTINCT it.item_id,
        aa.organization,
        iaf.countrycode,
        aa.author_seq_nr,
        (1/(MAX(author_seq_nr) OVER (PARTITION BY aa.item_id))::numeric)/(DENSE_RANK() OVER(PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization ASC) + DENSE_RANK() OVER (PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization DESC) - 1) AS orga_share
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_scp
        WHERE ind = 1
        ) it
      JOIN scp_b_202404.authors_affiliations aa
        ON aa.item_id = it.item_id
          AND organization IS NOT NULL
      JOIN scp_b_202404.items_affiliations iaf
        ON iaf.item_id = aa.item_id
          AND iaf.aff_seq_nr = aa.aff_seq_nr
          AND iaf.type = 'RS'
      )
    GROUP BY item_id, organization, countrycode
)
GROUP BY item_id, countrycode, 1;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for RS author
*/
INSERT INTO frctnl_cnt_scpb202404_cntrylvl
SELECT item_id, countrycode, SUM(orga_frak) AS frac_share, 2 -- aggregating on country level
FROM( -- aggregating on organization level
    SELECT DISTINCT iaf.item_id,
      iaf.organization,
      iaf.countrycode,
      1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
    FROM (
      SELECT item_id
      FROM tmp_items_iai_complete_scp
      WHERE ind = 2
    ) it
    JOIN scp_b_202404.items_affiliations iaf
      ON iaf.item_id = it.item_id
        AND iaf.type = 'RS'
        AND organization IS NOT NULL
  )
GROUP BY item_id, countrycode, 2;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/
INSERT INTO frctnl_cnt_scpb202404_cntrylvl
SELECT DISTINCT iaf.item_id,
  iaf.countrycode,
  1/DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.countrycode ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.countrycode DESC) - 1 AS frac_share, -- account for several RP authors from different countries
  3
FROM (
  SELECT item_id
  FROM tmp_items_iai_complete_scp
  WHERE ind = 3
) it
JOIN scp_b_202404.items_affiliations iaf
  ON iaf.item_id = it.item_id
    AND iaf.type = 'RP'
    AND countrycode IS NOT NULL;

CREATE INDEX frctnl_cnt_scpb202404_cntry_ix ON frctnl_cnt_scpb202404_cntrylvl (item_id, countrycode);

COMMIT;
```

### Testing

We run several test on the computed weights to observe their coverage
and their properness to serve as weights. The following numbers present
the respective percentage shares of indexed articles and reviews in
journals with questionable weights generated by the implementation for
*Scopus* detailed above.

|      | No weights available? | Inappropriate weights, i.e. smaller or bigger than (0,1\]? | More than one weight by country? | Weights missing for some country? | Sum of weights bigger than one? | Sum of weights smaller than one? |
|:-----|----------------------:|-----------------------------------------------------------:|---------------------------------:|----------------------------------:|--------------------------------:|---------------------------------:|
| 2000 |                   8.8 |                                                        0.4 |                                0 |                               0.6 |                             0.3 |                              2.0 |
| 2001 |                  10.4 |                                                        0.3 |                                0 |                               0.6 |                             0.3 |                              1.5 |
| 2002 |                  10.6 |                                                        0.3 |                                0 |                               0.9 |                             0.3 |                              1.3 |
| 2003 |                  10.1 |                                                        0.5 |                                0 |                               0.9 |                             0.4 |                              2.4 |
| 2004 |                   8.3 |                                                        0.2 |                                0 |                               0.8 |                             0.4 |                              1.5 |
| 2005 |                   6.6 |                                                        0.2 |                                0 |                               0.7 |                             0.4 |                              1.4 |
| 2006 |                   5.7 |                                                        0.2 |                                0 |                               0.7 |                             0.5 |                              1.4 |
| 2007 |                   5.5 |                                                        0.2 |                                0 |                               1.0 |                             0.6 |                              1.3 |
| 2008 |                   4.8 |                                                        0.2 |                                0 |                               1.7 |                             0.9 |                              1.3 |
| 2009 |                   4.4 |                                                        0.1 |                                0 |                               0.3 |                             0.6 |                              1.2 |
| 2010 |                   4.0 |                                                        0.1 |                                0 |                               0.4 |                             0.6 |                              1.2 |
| 2011 |                   3.6 |                                                        0.1 |                                0 |                               0.3 |                             0.6 |                              1.2 |
| 2012 |                   3.3 |                                                        0.1 |                                0 |                               0.3 |                             0.6 |                              1.1 |
| 2013 |                   3.2 |                                                        0.1 |                                0 |                               0.2 |                             0.7 |                              1.1 |
| 2014 |                   3.5 |                                                        0.2 |                                0 |                               0.2 |                             0.7 |                              1.3 |
| 2015 |                   3.3 |                                                        0.2 |                                0 |                               0.3 |                             0.8 |                              1.4 |
| 2016 |                   2.7 |                                                        0.3 |                                0 |                               0.2 |                             0.8 |                              1.5 |
| 2017 |                   2.2 |                                                        0.4 |                                0 |                               0.2 |                             0.9 |                              1.6 |
| 2018 |                   1.9 |                                                        0.5 |                                0 |                               0.2 |                             0.9 |                              1.7 |
| 2019 |                   1.6 |                                                        0.5 |                                0 |                               0.2 |                             1.0 |                              1.6 |
| 2020 |                   1.5 |                                                        0.5 |                                0 |                               0.1 |                             1.1 |                              1.6 |
| 2021 |                   1.4 |                                                        0.5 |                                0 |                               0.1 |                             1.1 |                              1.7 |
| 2022 |                   1.2 |                                                        0.5 |                                0 |                               0.1 |                             1.2 |                              1.6 |
| 2023 |                   1.2 |                                                        0.4 |                                0 |                               0.1 |                             1.2 |                              1.6 |

![](README_files/figure-gfm/tab_testing_scp_cntrylvl-1.png)<!-- -->

<!-- ## Comparison with ISI fractionation on organization1 -->
<!-- As an alternative implementation we also compute weights based on fractionation on the *organization1* level: -->
<!-- ```sql -->
<!-- /* -->
<!-- create empty table defining fractional weight for every paper by country -->
<!-- based on fractionation on organization1 level -->
<!-- */ -->
<!-- CREATE TABLE frctnl_cnt_scpb2019_org1 ( -->
<!--     fk_items NUMBER, -->
<!--     countrycode VARCHAR2(10 CHAR), -->
<!--     frac_share NUMBER(3,2) -->
<!-- ); -->
<!-- /* -->
<!-- include items with INCOMPLETE iai information via fractional counting on organization level: -->
<!-- */ -->
<!-- INSERT INTO frctnl_cnt_scpb2019_org1 -->
<!-- SELECT fk_items, countrycode, SUM(orga_frak) AS frac_share -- aggregating on country level -->
<!-- FROM( -- aggregating on organization1 level -->
<!--     SELECT fk_items, organization1, countrycode, SUM(orga_share)  AS orga_frak -->
<!--     FROM( -- computing fractional contribution on organisation level -->
<!--       SELECT DISTINCT fk_items, ORGANIZATION1, countrycode, -->
<!--         (1/(COUNT (DISTINCT organization1) OVER (PARTITION BY fk_items))) AS orga_share -->
<!--       FROM scopus_b_2020.items it -->
<!--       JOIN scopus_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--       JOIN scopus_b_2020.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--       WHERE pubtype = 'J' -->
<!--         AND doctype IN ('ar', 're') -->
<!--         AND pubyear BETWEEN 1995 AND 2019 -->
<!--         AND type = 'RS' -->
<!--         AND organization1 IS NOT NULL -->
<!--       ) -->
<!--     GROUP BY fk_items, organization1, countrycode -->
<!-- ) -->
<!-- GROUP BY fk_items, countrycode; -->
<!-- CREATE INDEX frctnl_cnt_scpb2019_org1_idx ON frctnl_cnt_scpb2019_org1 (fk_items, countrycode); -->
<!-- COMMIT; -->
<!-- ``` -->
<!-- Computing fractional counts of publications on national level for 2016: -->
<!-- ```{r, testing_scp_1, echo=FALSE, cache=TRUE, eval=FALSE} -->
<!-- # hybrid approach -->
<!-- pubs_cntry_whole_wos_frac_scp <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--     SELECT country, pubyear, SUM(frac_share) pub_cnt_frak -->
<!--     FROM( -->
<!--       SELECT DISTINCT it.pubyear, inst.countrycode country, it.pk_items, frak.frac_share -->
<!--       FROM scopus_b_2020.items it -->
<!--       JOIN scopus_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--       JOIN scopus_b_2020.institutions inst ON inst.pk_institutions=iai.fk_institutions -->
<!--       JOIN frctnl_cnt_scpb2020_cntrylvl frak ON frak.fk_items = it.pk_items -->
<!--         AND frak.countrycode = inst.countrycode -->
<!--       WHERE it.pubyear = 2016 -->
<!--         AND it.doctype IN ('ar','re') -->
<!--         AND it.pubtype = 'J' -->
<!--         AND inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA', -->
<!--                                    'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--     ) -->
<!--     GROUP BY country, pubyear -->
<!--     ORDER BY pub_cnt_frak DESC -->
<!--     "), -->
<!--     width = 100000, simplify=TRUE) -->
<!-- ) -->
<!-- ``` -->
<!-- ```{r, testing_scp_2, echo=FALSE, cache=TRUE, eval=FALSE} -->
<!-- # ISI approach -->
<!-- pubs_cntry_whole_wos_frac_ISI_scp <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--     SELECT country, pubyear, SUM(frac_share) pub_cnt_frak_ISI -->
<!--     FROM( -->
<!--       SELECT DISTINCT it.pubyear, inst.countrycode country, it.pk_items, frak.frac_share -->
<!--       FROM scopus_b_2020.items it -->
<!--       JOIN scopus_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--       JOIN scopus_b_2020.institutions inst ON inst.pk_institutions=iai.fk_institutions -->
<!--       JOIN frctnl_cnt_scpb2019_org1 frak ON frak.fk_items = it.pk_items -->
<!--         AND frak.countrycode = inst.countrycode -->
<!--       WHERE it.pubyear = 2016 -->
<!--         AND it.doctype IN ('ar','re') -->
<!--         AND it.pubtype = 'J' -->
<!--         AND inst.countrycode IN ('AUT', 'BEL', 'BRA', 'CAN', 'CHE', 'CHN', 'DNK', 'DEU', 'ESP', 'FIN', 'FRA', -->
<!--                                    'GBR', 'IND', 'ISR', 'ITA', 'JPN', 'KOR', 'NLD', 'POL', 'RUS', 'SWE', 'USA', 'ZAF') -->
<!--     ) -->
<!--     GROUP BY country, pubyear -->
<!--     ORDER BY pub_cnt_frak_ISI DESC -->
<!--     "), -->
<!--     width = 100000, simplify=TRUE) -->
<!-- ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE, cache=FALSE, eval=FALSE} -->
<!-- # table -->
<!-- pubs_cnt_compa <- cbind(pubs_cntry_whole_wos_frac_scp, pubs_cntry_whole_wos_frac_ISI_scp) -->
<!-- print(pubs_cnt_compa) -->
<!-- # kable(pubs_cnt_compa[1,3,4,6], -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- # kable(pubs_cntry_whole_wos_frac_ISI, -->
<!-- #       digits = 2 -->
<!-- #       ) -->
<!-- ``` -->

## Known Caveats

- Mega author papers with a large number of participating countries are
  excluded from any analysis applying these weights if the country
  specific fractional weights become too small and are rounded to zero.
- Data quality seems to be better in *Scopus*. However, the field
  `organization` is not as much structured in *Scopus* as in *Web of
  Science*, consequently as an alternative one could consider to use the
  *Scopus* institution harmonization *AFID* in the Rohdatenbank instead
  of `organization`. However an uniformly detailed affiliation
  assignment in `organization` might not have substantial effects on
  country statistics, while the `kb_inst_id` should jointly for all
  German institution include several of these detailed sub-organization
  affiliations and consequently validate a relative comparison among
  German institutions.

# Summing up

Fractionation on the author level relies on complete information on the
affiliations of single authors.

In the *Web of Science* the share of items with incomplete records
constantly constitutes a substantial part of the corpus. However data
quality has strongly improved since 2008 and again since 2013. Less than
5% of all articles and reviews are incomplete. Hence WoS data quality
apparently gets improved every year.

*Socpus* holds less incomplete records, their share has been below 11%
for all analysed years and below 5% since 2008.

Consequently fractionizing on the author level seems only reasonable on
complete records, whereas incomplete records might be fractionoized on
the `organization` level. Both approaches could be improved via a
worldwide institution harmonization, which has partially be done by ISI
Karlsruhe for their EFI reports.

# Appendix

## KB institution encoding

Fractionation weights are computed at the `organiazation` level (either
with prior fractionation at the `author_seq_nr` level or due to
incomplete information only on the `organization` level) and afterwards
transferred to the corresponding `kb_inst_id` via the `aff_seq_nr` link.
Consequently we assume that every `kb_inst_id` constitutes a superset of
`organization`.

### Implementation for wos_b_202404

``` sql

DROP TABLE frctnl_cnt_wosb202404_kbinstlvl_a;
/*
create empty table defining fractional weight for every paper by country
causes implicit commit
*/
CREATE TABLE frctnl_cnt_wosb202404_kbinstlvl_a (
    item_id VARCHAR(50),
    kb_inst_id SMALLINT,
    frac_share NUMERIC(4,3),
    ind_complete SMALLINT
);

/*
compute weight for items with COMPLETE iai information via fractional counting on author level:
*/
INSERT INTO frctnl_cnt_wosb202404_kbinstlvl_a
SELECT item_id, kb_inst_id, SUM(orga_share) AS frac_share, 1 -- aggregating on kb_inst_id level
FROM( -- aggregating on organization1 level
  SELECT DISTINCT tmp.item_id, tmp.author_seq_nr, tmp.organization, kbin.kb_inst_id, tmp.orga_share -- ignore distinct aff_seq_nr for same organization
  FROM( -- computing fractional contribution of organsation on author level
    SELECT DISTINCT it.item_id,
      aa.organization,
      aa.author_seq_nr,
      iaf.aff_seq_nr,
      (1/(MAX(author_seq_nr) OVER (PARTITION BY aa.item_id))::numeric)/(DENSE_RANK() OVER(PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization ASC) + DENSE_RANK() OVER (PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization DESC) - 1) AS orga_share
    FROM (
      SELECT item_id
      FROM tmp_items_iai_complete_wos
      WHERE ind = 1
      ) it
    JOIN wos_b_202404.authors_affiliations aa
      ON aa.item_id = it.item_id
        AND organization IS NOT NULL
    JOIN wos_b_202404.items_affiliations iaf
      ON iaf.item_id = aa.item_id
        AND iaf.aff_seq_nr = aa.aff_seq_nr
        AND iaf.type = 'RS'
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM wos_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr
)
GROUP BY item_id, kb_inst_id, 1;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for RS author
*/
INSERT INTO frctnl_cnt_wosb202404_kbinstlvl_a
SELECT item_id, kb_inst_id, SUM(orga_frak) AS frac_share, 2 -- aggregating on country level
FROM( -- aggregating on organization level
    SELECT DISTINCT tmp.item_id, organization, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    FROM( -- assign similar weight for every organization
      SELECT DISTINCT iaf.item_id,
        iaf.organization,
        iaf.aff_seq_nr,
        1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_wos
        WHERE ind = 2
      ) it
      JOIN wos_b_202404.items_affiliations iaf
        ON iaf.item_id = it.item_id
          AND iaf.type = 'RS'
          AND organization IS NOT NULL
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM wos_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr
  )
GROUP BY item_id, kb_inst_id, 2;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/
INSERT INTO frctnl_cnt_wosb202404_kbinstlvl_a
-- SELECT item_id, kb_inst_id, SUM(orga_frak) AS frac_share, 3 -- aggregating on country level
-- FROM( -- aggregating on organization level
--    SELECT DISTINCT tmp.item_id, organization, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    SELECT DISTINCT tmp.item_id, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    FROM( -- assign similar weight for every organization
      SELECT DISTINCT iaf.item_id,
        iaf.organization,
        iaf.aff_seq_nr,
        1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_wos
        WHERE ind = 3
      ) it
      JOIN wos_b_202404.items_affiliations iaf
        ON iaf.item_id = it.item_id
          AND iaf.type = 'RP'
          AND organization IS NOT NULL
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM wos_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr;
--  )
-- GROUP BY item_id, kb_inst_id, 3;

CREATE INDEX frctnl_cnt_wosb202404_kbinstlvl_ix ON frctnl_cnt_wosb202404_kbinstlvl_a (item_id, kb_inst_id);

COMMIT;
```

#### Testing

We run several test on the computed weights to observe their coverage
and their properness to serve as weights. The following numbers present
the respective percentage shares of indexed articles and reviews in
journals with questionable weights generated by the implementation for
*Web Of Science* [detailed above](#Implementation-for-wos_b_2020).

|      | No weights available? | Inappropriate weights, i.e. smaller or bigger than (0,1\]? | More than one weight by kb_inst_id? | Weights missing for some kb_inst_id? | Sum of weights bigger than one for DEU-only pubs? |
|:-----|----------------------:|-----------------------------------------------------------:|------------------------------------:|-------------------------------------:|--------------------------------------------------:|
| 2000 |                   0.0 |                                                       53.3 |                                   0 |                                  0.0 |                                               2.4 |
| 2001 |                   0.0 |                                                       55.5 |                                   0 |                                  0.0 |                                               2.8 |
| 2002 |                   0.0 |                                                       57.9 |                                   0 |                                  0.0 |                                               2.7 |
| 2003 |                   0.0 |                                                       60.2 |                                   0 |                                  0.0 |                                               2.8 |
| 2004 |                   0.0 |                                                       61.7 |                                   0 |                                  0.0 |                                               2.9 |
| 2005 |                   0.0 |                                                       63.5 |                                   0 |                                  0.0 |                                               3.0 |
| 2006 |                   0.0 |                                                       64.6 |                                   0 |                                  0.0 |                                               3.0 |
| 2007 |                   0.0 |                                                       59.5 |                                   0 |                                  0.0 |                                               3.6 |
| 2008 |                   0.1 |                                                        3.7 |                                   0 |                                  0.1 |                                               7.9 |
| 2009 |                   0.0 |                                                        3.4 |                                   0 |                                  0.0 |                                               8.1 |
| 2010 |                   0.0 |                                                        3.5 |                                   0 |                                  0.0 |                                               8.6 |
| 2011 |                   0.0 |                                                        3.5 |                                   0 |                                  0.0 |                                               8.7 |
| 2012 |                   0.0 |                                                        3.5 |                                   0 |                                  0.0 |                                               8.6 |
| 2013 |                   0.0 |                                                        3.0 |                                   0 |                                  0.0 |                                               9.5 |
| 2014 |                   0.0 |                                                        2.9 |                                   0 |                                  0.0 |                                              10.4 |
| 2015 |                   0.0 |                                                        2.8 |                                   0 |                                  0.0 |                                              10.9 |
| 2016 |                   0.0 |                                                        2.8 |                                   0 |                                  0.1 |                                              11.4 |
| 2017 |                   0.0 |                                                        2.9 |                                   0 |                                  0.1 |                                              11.7 |
| 2018 |                   0.0 |                                                        2.9 |                                   0 |                                  0.1 |                                              12.1 |
| 2019 |                   0.0 |                                                        2.8 |                                   0 |                                  0.0 |                                              12.2 |
| 2020 |                   0.0 |                                                        2.5 |                                   0 |                                  0.0 |                                              13.1 |
| 2021 |                   0.0 |                                                        2.6 |                                   0 |                                  0.0 |                                              14.1 |
| 2022 |                   0.0 |                                                        2.8 |                                   0 |                                  0.1 |                                              14.6 |
| 2023 |                   0.0 |                                                        3.4 |                                   0 |                                  0.1 |                                              14.1 |

![](README_files/figure-gfm/tab_testing_wos_kbinstlvl-1.png)<!-- -->

### Implementation for scopus_b_202404

``` sql

DROP TABLE frctnl_cnt_scpb202404_kbinstlvl_a;
/*
create empty table defining fractional weight for every paper by country
causes implicit commit
*/
CREATE TABLE frctnl_cnt_scpb202404_kbinstlvl_a (
    item_id VARCHAR(50),
    kb_inst_id SMALLINT,
    frac_share NUMERIC(4,3),
    ind_complete SMALLINT
);

/*
compute weight for items with COMPLETE iai information via fractional counting on author level:
*/
INSERT INTO frctnl_cnt_scpb202404_kbinstlvl_a
SELECT item_id, kb_inst_id, SUM(orga_share) AS frac_share, 1 -- aggregating on kb_inst_id level
FROM( -- aggregating on organization1 level
  SELECT DISTINCT tmp.item_id, tmp.author_seq_nr, tmp.organization, kbin.kb_inst_id, tmp.orga_share -- ignore distinct aff_seq_nr for same organization
  FROM( -- computing fractional contribution of organsation on author level
    SELECT DISTINCT it.item_id,
      aa.organization,
      aa.author_seq_nr,
      iaf.aff_seq_nr,
      (1/(MAX(author_seq_nr) OVER (PARTITION BY aa.item_id))::numeric)/(DENSE_RANK() OVER(PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization ASC) + DENSE_RANK() OVER (PARTITION BY aa.item_id, aa.author_seq_nr ORDER BY aa.organization DESC) - 1) AS orga_share
    FROM (
      SELECT item_id
      FROM tmp_items_iai_complete_scp
      WHERE ind = 1
      ) it
    JOIN scp_b_202404.authors_affiliations aa
      ON aa.item_id = it.item_id
        AND organization IS NOT NULL
    JOIN scp_b_202404.items_affiliations iaf
      ON iaf.item_id = aa.item_id
        AND iaf.aff_seq_nr = aa.aff_seq_nr
        AND iaf.type = 'RS'
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM scp_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr
)
GROUP BY item_id, kb_inst_id, 1;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for RS author
*/
INSERT INTO frctnl_cnt_scpb202404_kbinstlvl_a
SELECT item_id, kb_inst_id, SUM(orga_frak) AS frac_share, 2 -- aggregating on country level
FROM( -- aggregating on organization level
    SELECT DISTINCT tmp.item_id, organization, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    FROM( -- assign similar weight for every organization
      SELECT DISTINCT iaf.item_id,
        iaf.organization,
        iaf.aff_seq_nr,
        1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_scp
        WHERE ind = 2
      ) it
      JOIN scp_b_202404.items_affiliations iaf
        ON iaf.item_id = it.item_id
          AND iaf.type = 'RS'
          AND organization IS NOT NULL
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM scp_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr
  )
GROUP BY item_id, kb_inst_id, 2;

/*
compute weights for items with INCOMPLETE iai information and feasible link to institution table for only RP author
*/
INSERT INTO frctnl_cnt_scpb202404_kbinstlvl_a
-- SELECT item_id, kb_inst_id, SUM(orga_frak) AS frac_share, 3 -- aggregating on country level
-- FROM( -- aggregating on organization level
--    SELECT DISTINCT tmp.item_id, organization, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    SELECT DISTINCT tmp.item_id, kb_inst_id, orga_frak -- ignore distinct fk_institution for same organization1
    FROM( -- assign similar weight for every organization
      SELECT DISTINCT iaf.item_id,
        iaf.organization,
        iaf.aff_seq_nr,
        1/(DENSE_RANK() OVER(PARTITION BY iaf.item_id ORDER BY iaf.organization ASC) + DENSE_RANK() OVER (PARTITION BY iaf.item_id ORDER BY iaf.organization DESC) - 1) AS orga_frak
      FROM (
        SELECT item_id
        FROM tmp_items_iai_complete_scp
        WHERE ind = 3
      ) it
      JOIN scp_b_202404.items_affiliations iaf
        ON iaf.item_id = it.item_id
          AND iaf.type = 'RP'
          AND organization IS NOT NULL
    ) tmp
  JOIN( -- link aff_seq_nr to kb_inst
    SELECT DISTINCT item_id, kb_inst_id, aff_seq_nr
    FROM scp_b_202404.kb_a_addr_inst_sec
    ) kbin
    ON kbin.item_id = tmp.item_id
      AND kbin.aff_seq_nr = tmp.aff_seq_nr;
--  )
-- GROUP BY item_id, kb_inst_id, 3;

CREATE INDEX frctnl_cnt_scpb202404_kbinstlvl_ix ON frctnl_cnt_scpb202404_kbinstlvl_a (item_id, kb_inst_id);

COMMIT;
```

#### Testing

We run several test on the computed weights to observe their coverage
and their properness to serve as weights. The following numbers present
the respective percentage shares of indexed articles and reviews in
journals with questionable weights generated by the implementation for
*Scopus* [detailed above](#Implementation-for-scopus_b_2020).

|      | No weights available? | Inappropriate weights, i.e. smaller or bigger than (0,1\]? | More than one weight by kb_inst_id? | Weights missing for some kb_inst_id? | Sum of weights bigger than one for DEU-only pubs? |
|:-----|----------------------:|-----------------------------------------------------------:|------------------------------------:|-------------------------------------:|--------------------------------------------------:|
| 2000 |                   0.3 |                                                        0.6 |                                   0 |                                  0.1 |                                               5.4 |
| 2001 |                   0.4 |                                                        0.3 |                                   0 |                                  0.1 |                                               5.8 |
| 2002 |                   1.0 |                                                        0.4 |                                   0 |                                  0.1 |                                               6.2 |
| 2003 |                   1.0 |                                                        0.7 |                                   0 |                                  0.1 |                                               6.5 |
| 2004 |                   0.9 |                                                        0.3 |                                   0 |                                  0.1 |                                               6.5 |
| 2005 |                   0.8 |                                                        0.2 |                                   0 |                                  0.1 |                                               7.0 |
| 2006 |                   1.1 |                                                        0.2 |                                   0 |                                  0.1 |                                               7.2 |
| 2007 |                   1.2 |                                                        0.2 |                                   0 |                                  0.1 |                                               7.4 |
| 2008 |                   1.0 |                                                        0.2 |                                   0 |                                  0.1 |                                               7.0 |
| 2009 |                   0.8 |                                                        0.2 |                                   0 |                                  0.1 |                                               7.3 |
| 2010 |                   1.0 |                                                        0.1 |                                   0 |                                  0.1 |                                               8.0 |
| 2011 |                   0.9 |                                                        0.1 |                                   0 |                                  0.2 |                                               8.2 |
| 2012 |                   0.7 |                                                        0.2 |                                   0 |                                  0.4 |                                               8.1 |
| 2013 |                   0.6 |                                                        0.2 |                                   0 |                                  1.0 |                                               8.6 |
| 2014 |                   0.6 |                                                        0.4 |                                   0 |                                  0.2 |                                               9.4 |
| 2015 |                   0.7 |                                                        0.6 |                                   0 |                                  0.3 |                                               9.6 |
| 2016 |                   0.7 |                                                        0.9 |                                   0 |                                  0.3 |                                               9.7 |
| 2017 |                   0.6 |                                                        1.1 |                                   0 |                                  0.3 |                                              10.1 |
| 2018 |                   0.6 |                                                        1.3 |                                   0 |                                  0.3 |                                              10.6 |
| 2019 |                   0.1 |                                                        1.5 |                                   0 |                                  0.5 |                                              10.9 |
| 2020 |                   0.0 |                                                        1.3 |                                   0 |                                  0.6 |                                              11.5 |
| 2021 |                   0.0 |                                                        1.5 |                                   0 |                                  0.5 |                                              12.3 |
| 2022 |                   0.0 |                                                        1.6 |                                   0 |                                  0.5 |                                              12.6 |
| 2023 |                   0.0 |                                                        1.5 |                                   0 |                                  0.8 |                                              12.3 |

![](README_files/figure-gfm/tab_testing_scp_kbinstlvl-1.png)<!-- -->

<!-- ## Conference Proceedings Web of Science -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings pro Jahr -->
<!-- cpByYear <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT pk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   WHERE doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # inner vs left join between items and items_authors_institutions -->
<!-- cp_outerInneDiff <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!-- SELECT tmp1.pubyear, (tmp2.count_outer - tmp1.count_inner) AS pub_diff -->
<!--   FROM( -->
<!--     SELECT pubyear, COUNT(DISTINCT iai.fk_items) count_inner -->
<!--     FROM wos_b_2020.items it -->
<!--     JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--     WHERE doctype = 'Proceedings Paper' -->
<!--       AND pubyear BETWEEN 1995 AND 2019 -->
<!--     GROUP BY pubyear) tmp1 -->
<!--   JOIN( -->
<!--     SELECT pubyear, COUNT(DISTINCT iai.fk_items) count_outer -->
<!--     FROM wos_b_2020.items it -->
<!--     LEFT JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items=iai.fk_items -->
<!--     WHERE doctype = 'Proceedings Paper' -->
<!--       AND pubyear BETWEEN 1995 AND 2019 -->
<!--     GROUP BY pubyear) tmp2 ON tmp1.pubyear = tmp2.pubyear -->
<!--   ORDER BY tmp1.pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   )  -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings mit fk_authors als NULL -->
<!-- cp_paperNullAuthor <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT fk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   WHERE fk_authors IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--     AND (type = 'RS' OR type IS NULL) -->
<!--     AND (role = 'author' OR role IS NULL) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings mit fk_institutions als NULL -->
<!-- cp_paperNullInstitution <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT fk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   WHERE fk_institutions IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--     AND (type = 'RS' OR type IS NULL) -->
<!--     AND (role = 'author' OR role IS NULL) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings with no information on type -->
<!-- cp_paperNullType <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT fk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   WHERE type IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- if(dim(cp_paperNullType)[1]==0){ -->
<!--   cp_paperNullType <- as.data.frame(matrix( -->
<!--     c(1995:2019, rep(0,24)), -->
<!--     nrow=24, -->
<!--     ncol=2 -->
<!--   )) -->
<!-- } -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings with no information on role -->
<!-- cp_paperNullRole <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT fk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   WHERE role IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # Proceedings with no information on organization1 -->
<!-- cp_paperNullorganization1 <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(DISTINCT fk_items) -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   JOIN wos_b_2020.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--   WHERE organization1 IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- ```{r, echo=FALSE} -->
<!-- # all potential missing information jointly -->
<!-- cp_paperOverall <- dbGetQuery(fraction_pool, strwrap(paste0(" -->
<!--   SELECT pubyear, COUNT(pk_items) -->
<!--   FROM( -->
<!--   SELECT DISTINCT pubyear, pk_items -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.items_authors_institutions iai ON it.pk_items = iai.fk_items -->
<!--   WHERE doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--     AND ((fk_institutions IS NULL -->
<!--           AND (type = 'RS' OR type IS NULL) AND (role = 'author' OR role IS NULL)) OR -->
<!--         (fk_authors IS NULL -->
<!--           AND (type = 'RS' OR type IS NULL) AND (role = 'author' OR role IS NULL)) OR -->
<!--         (type IS NULL) OR -->
<!--         (role IS NULL)) -->
<!--   UNION -->
<!--   SELECT DISTINCT pubyear,  pk_items -->
<!--   FROM wos_b_2020.items it -->
<!--   JOIN wos_b_2020.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--   JOIN wos_b_2020.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--   WHERE organization1 IS NULL -->
<!--     AND doctype = 'Proceedings Paper' -->
<!--     AND pubyear BETWEEN 1995 AND 2019 -->
<!--   ) -->
<!--   GROUP BY pubyear -->
<!--   ORDER BY pubyear ASC -->
<!--   "), -->
<!--   width = 100000, simplify = TRUE) -->
<!--   ) -->
<!-- ``` -->
<!-- Percentages of pk_items with partially missing information: -->
<!-- ```{r, cp_tab_friction, echo=FALSE, cache=FALSE} -->
<!-- # print(outerInneDiff[,2]) -->
<!-- # print(paperOverall[,2]/papersByYear[,2]) -->
<!-- # print((paperOverall[,2] + outerInneDiff[,2])/papersByYear[,2]) -->
<!-- cp_tab_friction <- as.matrix(cbind(cp_outerInneDiff[,2]/cpByYear[,2], -->
<!--                                 cp_paperNullType[,2]/cpByYear[,2], -->
<!--                                 cp_paperNullRole[,2]/cpByYear[,2], -->
<!--                                 cp_paperNullInstitution[,2]/cpByYear[,2], -->
<!--                                 cp_paperNullAuthor[,2]/cpByYear[,2], -->
<!--                                 cp_paperNullorganization1[,2]/cpByYear[,2], -->
<!--                                 (cp_paperOverall[,2] + cp_outerInneDiff[,2])/cpByYear[,2])*100) -->
<!-- colnames(cp_tab_friction) <- c("only pk_item ", -->
<!--                             "no type", -->
<!--                             "no role", -->
<!--                             "no affiliation", -->
<!--                             "no author", -->
<!--                             "No Organization1", -->
<!--                             "total") -->
<!-- rownames(cp_tab_friction) <- as.character(1995:2019) -->
<!-- kable(cp_tab_friction, -->
<!--       digits = 1 -->
<!--       ) -->
<!-- matplot(1995:2019, cp_tab_friction, -->
<!--      xlim=c(1995,2020), -->
<!--      ylim=c(0,100), -->
<!--      lwd=2, -->
<!--      lty = 1, -->
<!--      bty="n", -->
<!--      xlab="", -->
<!--      ylab="%", -->
<!--      type="l", -->
<!--      col = rev(viridis(7))) -->
<!-- legend("topright", legend = rev(colnames(cp_tab_friction)), fill=viridis(7), bty = "n") -->
<!-- ``` -->
<!-- Implementation: -->
<!-- ```sql -->
<!-- /* -->
<!-- populate temporary table -->
<!-- */ -->
<!-- INSERT INTO tmp_items_iai_complete ( -->
<!-- SELECT DISTINCT tmp.fk_items -- subset of items with complete iai information -->
<!-- FROM( -- counts numbers of entries in iai for every item -->
<!--     SELECT fk_items, COUNT (*) AS entry_cnt -->
<!--     FROM wos_b_2018.items it -->
<!--     LEFT JOIN wos_b_2018.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--     LEFT JOIN wos_b_2018.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--     WHERE (role IS NULL OR role = 'author') -- we only care for authors and missing information, other roles are neglected -->
<!--         AND doctype = 'Proceedings Paper' -->
<!--         AND pubyear BETWEEN 2007 AND 2017 -->
<!--     GROUP BY fk_items -->
<!-- ) tmp -->
<!-- JOIN( -- counts numbers of COMPLETE entries in iai for every item -->
<!--     SELECT fk_items, COUNT (*) AS entry_cnt -->
<!--     FROM wos_b_2018.items it -->
<!--     LEFT JOIN wos_b_2018.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--     LEFT JOIN wos_b_2018.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--     WHERE doctype = 'Proceedings Paper' -->
<!--         AND pubyear BETWEEN 2007 AND 2017 -->
<!--         AND fk_institutions IS NOT NULL -->
<!--         AND fk_authors IS NOT NULL -->
<!--         AND role = 'author' -->
<!--         AND type IS NOT NULL -->
<!--     GROUP BY fk_items -->
<!-- ) tmp2 ON tmp.fk_items = tmp2.fk_items -->
<!-- AND tmp.entry_cnt = tmp2.entry_cnt); -- compare both numbers -->
<!-- /* -->
<!-- include items with COMPLETE iai information via fractional counting on author level: -->
<!-- */ -->
<!-- INSERT INTO wosb2018_frc_cntrylvl -->
<!-- SELECT fk_items, countrycode, SUM(orga_frak) AS frac_share, 1 -- aggregating on country level -->
<!-- FROM( -- aggregating on organization1 level -->
<!--     SELECT fk_items, organization1, countrycode, SUM(orga_share)  AS orga_frak -->
<!--     FROM( -- computing fractional contribution of organsation on author level -->
<!--       SELECT DISTINCT fk_items, ORGANIZATION1, countrycode, fk_authors, -->
<!--         (1/(COUNT (DISTINCT fk_authors) OVER (PARTITION BY fk_items))) -->
<!--         /(COUNT (DISTINCT organization1) OVER (PARTITION BY fk_items, fk_authors)) AS orga_share -->
<!--       FROM wos_b_2018.items it -->
<!--       JOIN wos_b_2018.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--       JOIN wos_b_2018.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--       WHERE doctype = 'Proceedings Paper' -->
<!--         AND pubyear BETWEEN 2007 AND 2017 -->
<!--         AND role = 'author' -->
<!--         AND type = 'RS' -->
<!--         AND pk_items IN (SELECT fk_items FROM tmp_items_iai_complete) -->
<!--         AND organization1 IS NOT NULL -->
<!--       ) -->
<!--     GROUP BY fk_items, organization1, countrycode -->
<!-- ) -->
<!-- GROUP BY fk_items, countrycode, 1; -->
<!-- /* -->
<!-- include items with INCOMPLETE iai information via fractional counting on organization level: -->
<!-- */ -->
<!-- INSERT INTO wosb2018_frc_cntrylvl -->
<!-- SELECT fk_items, countrycode, SUM(orga_frak) AS frac_share, 0 -- aggregating on country level -->
<!-- FROM( -- aggregating on organization1 level -->
<!--     SELECT fk_items, organization1, countrycode, SUM(orga_share)  AS orga_frak -->
<!--     FROM( -- computing fractional contribution on organisation level -->
<!--       SELECT DISTINCT fk_items, ORGANIZATION1, countrycode, -->
<!--         (1/(COUNT (DISTINCT organization1) OVER (PARTITION BY fk_items))) AS orga_share -->
<!--       FROM wos_b_2018.items it -->
<!--       JOIN wos_b_2018.ITEMS_AUTHORS_INSTITUTIONS iai ON it.pk_items = iai.fk_items -->
<!--       JOIN wos_b_2018.institutions inst ON inst.pk_institutions = iai.fk_institutions -->
<!--       WHERE doctype = 'Proceedings Paper' -->
<!--         AND pubyear BETWEEN 2007 AND 2017 -->
<!--         AND (role = 'author' OR role IS NULL) -->
<!--         AND type = 'RS' -->
<!--         AND pk_items NOT IN (SELECT fk_items FROM tmp_items_iai_complete) -->
<!--         AND organization1 IS NOT NULL -->
<!--       ) -->
<!--     GROUP BY fk_items, organization1, countrycode -->
<!-- ) -->
<!-- GROUP BY fk_items, countrycode, 0; -->
<!-- COMMIT; -->
<!-- DROP INDEX wosb2018_frc_cntrylvl_ix; -->
<!-- CREATE INDEX wosb2018_frc_cntrylvl_ix ON wosb2018_frc_cntrylvl (fk_items, countrycode); -->
<!-- COMMIT; -->
<!-- ``` -->

``` sql
DROP TABLE tmp_items_iai_complete_wos;
DROP TABLE tmp_items_iai_complete_scp;
```

    ## Warning: You still have checked out objects. Return them to the pool so they
    ## can be destroyed. (If these are leaked objects - no reference - they will be
    ## destroyed the next time the garbage collector runs).

# Literatur

<div id="refs" class="references csl-bib-body hanging-indent"
line-spacing="2">

<div id="ref-waltman_field-normalized_2015" class="csl-entry">

Waltman, L. and Eck, N.J. van. (2015), “[Field-normalized citation
impact indicators and the choice of an appropriate counting
method](https://doi.org/10.1016/j.joi.2015.08.001)”, *Journal of
Informetrics*, Vol. 9 No. 4, pp. 872–894.

</div>

</div>
